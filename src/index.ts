import React from "react";
import ReactDOM from "react-dom";
import { AppComponent } from "./AppComponent";

ReactDOM.render(React.createElement(AppComponent), document.querySelector("body"));