import { Grid, Card, Typography, TextField, Button, CardContent, ListItem } from "@material-ui/core";
import React, { useState } from "react";

export function AppComponent(props = {}) {

    const notes = [2000, 500, 200, 100, 50, 20, 10, 5, 2, 1];

    const [amt, setAmt] = useState("0");
    const [denominations, setDenominations] = useState<any[]>([]);

    const getCount = () => {
        const { denominations } = notes.reduce(({ denominations, remaining }, name) => {
            if (name <= Number(amt)) {
                const count = Math.floor(remaining / name);
                const remains = remaining % name;
                denominations.push({ count, name });
                return { denominations, remaining: remains };
            } else {
                denominations.push({ count: 0, name });
                return { denominations, remaining };
            }
        }, {
            denominations: [],
            remaining: Number(amt)
        });
        setDenominations(denominations);
    }

    return <Grid container className="app-component" alignItems="center" justify="center">
        <Grid container item md={4}>
            <Card className="w-100">
                <CardContent>
                    <Grid container direction="column" className="p-2-all">
                        <Typography variant="h4" align="center">Welcome to ATM</Typography>
                        <Grid item>
                            <TextField type="number" fullWidth value={amt} onChange={e => setAmt(e.target.value)} label="Enter the Amount"></TextField>
                        </Grid>
                        <Grid item>
                            <Button onClick={getCount} variant="contained" color="primary" fullWidth>Get Money</Button>
                        </Grid>
                    </Grid>
                </CardContent>
            </Card>
        </Grid>
        <Grid container item md={4} direction="column">
            <ListItem className="font-weight-bold">You will get the following amount</ListItem>
            <Grid container>
                {
                    [...denominations].reverse().map(denomination =>
                        <Grid item md={6} key={denomination.name}>
                            <ListItem divider>
                                {denomination.count} of Rs {denomination.name}
                            </ListItem>
                        </Grid>
                    )
                }
            </Grid>
            <ListItem className="font-weight-bold">Total notes dispensed: {denominations.reduce((sum, { count }) => sum + count, 0)}</ListItem>
        </Grid>
    </Grid>
}