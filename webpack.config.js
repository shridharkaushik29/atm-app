const path = require("path");

module.exports = () => {

    const mode = "development";

    let src = path.resolve(__dirname, `src/`);

    return {
        mode,
        entry: [
            path.resolve(src, "index.ts"),
            `!file-loader?name=style.css!extract-loader!css-loader!sass-loader!${path.resolve(src, "index.scss")}`
        ],
        optimization: {
            runtimeChunk: "single",
            splitChunks: {
                cacheGroups: {
                    vendor: {
                        test: /node_modules/,
                        name: "vendor",
                        enforce: true,
                        chunks: "initial"
                    }
                }
            }
        },
        module: {
            rules: [
                {
                    test: /\.ts(x?)$/,
                    use: [
                        {
                            loader: "ts-loader",
                            options: {
                                transpileOnly: true,
                                experimentalWatchApi: true
                            }
                        }
                    ]
                },
                {
                    test: /\.scss$/,
                    use: ["sass-loader"]
                },
                {
                    test: /\.(svg|jpg|png|jpeg|gif|eot|woff|ttf)/,
                    use: "file-loader"
                }
            ]
        },
        resolve: {
            extensions: ['.js', '.ts', '.tsx']
        }
    }
}